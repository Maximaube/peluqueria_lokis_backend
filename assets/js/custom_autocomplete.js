;(function ($) {

//--------------------------------- SERVICIOS --------------------------------------------------------------------
    //Traigo el listado ul de servicios
    var listadoServicios = document.querySelector('select[name="servicios"]').nextSibling.getElementsByClassName("select2-selection__choice");
    //Utilizo el tamaño para saber si estoy agregando o eliminando elementos del listado
    var tamanioServicios = 0;
    
    if(listadoServicios.length>0){
        for(var i=0; i<listadoServicios.length; i++){
            var codigo = listadoServicios[i].innerText.substr(1);
            getServicio(codigo);
        }
        tamanioServicios = listadoServicios.length;
    }

    //Evento que captura un cambio en el select al seleccionar un elemento de la lista Servicios
    document.querySelector('select[name="servicios"]').onchange=function(event) {
        var options = $(this).find("option");

        // Aqui compara el array options con el array listadoServicios para ver
        // que elementos estan en uno y no en otro, y asi saber cual eliminar, ya que
        // al remover un elemento con autocomplete, se elimina de la lista pero no
        // de los options
        var i=0;
        var bandera = true;
        while(bandera && i<options.length){
            var codigo = options[i].value;

            // Si el listado es null o undefined quiere decir que no hay elementos en el listado
            // entonces elimino el option sobrante
            if(listadoServicios[i]===undefined || listadoServicios[i]===null){
                $("select[name='servicios'] option[value="+codigo+"]").remove();
                $('#cuerpo_tabla_servicios .'+codigo+'').remove();
                bandera=false;
                calcularTotal();
            }else{
                // Si encuentra un option que sea distinto del listado, lo elimina
                if(codigo!=listadoServicios[i].innerText.substr(1)){
                    $("select[name='servicios'] option[value="+codigo+"]").remove();
                    $('#cuerpo_tabla_servicios .'+codigo+'').remove();
                    bandera=false;
                    calcularTotal();
                }
            }
            i++;
        }

        tamanioServicios++;
        if(listadoServicios.length===tamanioServicios){
//          console.log("Agrega");
            var codigo = listadoServicios[listadoServicios.length-1].innerText.substr(1);
            getServicio(codigo);

        }else{
//          console.log("Elimina");
            tamanioServicios = listadoServicios.length;
        }
        
    };


    // Con el codigo obtengo el objeto Servicio mediante api rest
    function getServicio(codigo){
		$.ajax({
			method: 'GET',
			url: "http://localhost:8000/servicio/api/v1/servicio/"+codigo,
			success: function(data, status, xho){
				agregarFilaTablaServicios(data);
			},
			error: function(data){
				console.log("Error!");
				console.log(data);
			}
		});
	};


    // Agrego con HTML una fila de la tabla Servicios
	function agregarFilaTablaServicios(dato){
		$('#cuerpo_tabla_servicios').prepend(
			"<tr class='"+dato.codigo+"'>"+
                    "<td>"+dato.codigo+"</td>"+
                    "<td>"+dato.nombre+"</td>"+
                    "<td class='precio_tabla' >"+dato.precio+"</td>"+
                    "<td>"+dato.descripcion+"</td>"+
                    "<td>"+
                         "<button type='button' class='btn btn-icons btn-rounded btn-inverse-danger eliminarServicio' title='Eliminar servicio.'>"+
                             "<i class='mdi mdi-delete'></i>"+
                         "</button></td>"+
            "</tr>"
        );
        agregarEventoEliminarServicio();
	};


    // Asocio el evento click al boton eliminar en la fila recien creada en la tabla Servicios
    function agregarEventoEliminarServicio(){
        calcularTotal();
        $('.eliminarServicio').unbind();
        $('.eliminarServicio').click(function(){
            var codigo = $(this).parents("tr").attr('class');
            $(this).parents("tr").remove();
            $("select[name='servicios'] option[value="+codigo+"]").remove();
            tamanioServicios = listadoServicios.length-1;
            calcularTotal();
        });
    };

//--------------------------------- PRODUCTOS --------------------------------------------------------------------
    
    var listadoProductos = document.querySelector('select[name="productos"]').nextSibling.getElementsByClassName("select2-selection__choice");
    var tamanioProductos = 0;

    if(listadoProductos.length>0){
        for(var i=0; i<listadoProductos.length; i++){
            var codigo = listadoProductos[i].innerText.substr(1);
            getProducto(codigo);
        }
        tamanioProductos = listadoProductos.length;
    }

    //Evento que captura un cambio en el select al seleccionar un elemento de la lista Productos
    document.querySelector('select[name="productos"]').onchange=function(event) {
        var options = $(this).find("option");

        // Aqui compara el array options con el array listadoProductos para ver
        // que elementos estan en uno y no en otro, y asi saber cual eliminar, ya que
        // al remover un elemento con autocomplete, se elimina de la lista pero no
        // de los options
        var i=0;
        var bandera = true;
        while(bandera && i<options.length){
            var codigo = options[i].value;

            // Si el listado es null o undefined quiere decir que no hay elementos en el listado
            // entonces elimino el option sobrante
            if(listadoProductos[i]===undefined || listadoProductos[i]===null){
                $("select[name='productos'] option[value="+codigo+"]").remove();
                $('#cuerpo_tabla_productos .'+codigo+'').remove();
                bandera=false;
                calcularTotal();
            }else{
                // Si encuentra un option que sea distinto del listado, lo elimina
                if(codigo!=listadoProductos[i].innerText.substr(1)){
                    $("select[name='productos'] option[value="+codigo+"]").remove();
                    $('#cuerpo_tabla_productos .'+codigo+'').remove();
                    bandera=false;
                    calcularTotal();
                }
            }
            i++;
        }

        tamanioProductos++;
        if(listadoProductos.length===tamanioProductos){
//          console.log("Agrega");
            var codigo = listadoProductos[listadoProductos.length-1].innerText.substr(1);
            getProducto(codigo);
        }else{
//          console.log("Elimina");
            tamanioProductos = listadoProductos.length;
        }

    };

    // Con el codigo obtengo el objeto Producto mediante api rest
    function getProducto(codigo){
        $.ajax({
            method: 'GET',
            url: "http://localhost:8000/producto/api/v1/producto/"+codigo,
            success: function(data, status, xho){
                agregarFilaTablaProductos(data);
            },
            error: function(data){
                console.log("Error!");
                console.log(data);
            }
        });
    };

    // Agrego con HTML una fila de la tabla Productos
    function agregarFilaTablaProductos(dato){
        $('#cuerpo_tabla_productos').prepend(
            "<tr class='"+dato.codigo+"'>"+
                    "<td>"+dato.codigo+"</td>"+
                    "<td>"+dato.nombre+"</td>"+
                    "<td class='precio_tabla' >"+dato.precio+"</td>"+
                    "<td>"+dato.descripcion+"</td>"+
                    "<td>"+
                         "<button type='button' class='btn btn-icons btn-rounded btn-inverse-danger eliminarProducto' title='Eliminar producto.'>"+
                             "<i class='mdi mdi-delete'></i>"+
                         "</button></td>"+
            "</tr>"
        );
        agregarEventoEliminarProducto();
    };

    // Asocio el evento click al boton eliminar en la fila recien creada en la tabla Productos
    function agregarEventoEliminarProducto(){
        calcularTotal();
        $('.eliminarProducto').unbind();
        $('.eliminarProducto').click(function(){
            var codigo = $(this).parents("tr").attr('class');
            $(this).parents("tr").remove();
            $("select[name='productos'] option[value="+codigo+"]").remove();
            tamanioProductos = listadoProductos.length-1;
            calcularTotal();
        });
    };

//--------------------------------- CALCULO DE TOTAL -----------------------------------------------------------------
    function calcularTotal(){
        var listaServicios = $('#cuerpo_tabla_servicios').find('.precio_tabla');
        var listaProductos = $('#cuerpo_tabla_productos').find('.precio_tabla');
        var total = 0;
        for(var i=0; i<listaServicios.length; i++){
            total = total + Number(listaServicios[i].textContent);
        }
        for(var i=0; i<listaProductos.length; i++){
            total = total + Number(listaProductos[i].textContent);
        }
        $('#id_monto').val(total);
    };


})(yl.jQuery);