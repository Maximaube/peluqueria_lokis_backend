# -*- coding: utf-8 -*-
from django import forms

from apps.usuarios.models import Cliente
from .models import Venta, Turno
from dal import autocomplete


class VentaForm(forms.ModelForm):
    class Meta:
        model = Venta
        fields = ('cliente', 'fecha', 'hora', 'profesional',
                  'servicios', 'productos', 'metodo_pago', 'descuento', 'monto', 'para_imprimir')

        # labels = {
        #     'fecha_hora': 'Fecha y Hora',
        # }

        widgets = {
            'cliente': autocomplete.ModelSelect2(url='cliente-autocomplete', attrs={'class': 'form-control', 'data-html': True}),
            'fecha': forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}),
            'hora': forms.TimeInput(format="%H:%M", attrs={'class': 'form-control', 'type': 'time'}),
            'profesional': forms.Select(attrs={'class': 'form-control'}),
            'servicios': autocomplete.ModelSelect2Multiple(url='servicios-autocomplete', attrs={'class': 'form-control', 'placeholder': 'Buscar Servicio', 'data-html': True}),
            'productos': autocomplete.ModelSelect2Multiple(url='productos-autocomplete', attrs={'class': 'form-control', 'placeholder': 'Buscar Producto', 'data-html': True}),
            'metodo_pago': forms.Select(attrs={'class': 'form-control'}),
            'descuento': forms.NumberInput(attrs={'class': 'form-control'}),
            'monto': forms.NumberInput(attrs={'class': 'form-control'}),
            'para_imprimir': forms.CheckboxInput(attrs={'class': 'form-control'}),
        }



class TurnoForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = ('profesional', 'cliente', 'fecha', 'hora', 'descripcion', 'proposito')

        labels = {
            'profesional': 'Profesional',
            'cliente': 'Cliente',
            'fecha': 'Fecha',
            'hora': 'Hora',
            'descripcion': 'Descripcion',
            'proposito': 'Proposito',
        }

        widgets = {
            'profesional': forms.Select(attrs={'class': 'form-control'}),
            'cliente': autocomplete.ModelSelect2(url='cliente-autocomplete', attrs={'class': 'form-control', 'data-html': True}),
            'fecha': forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}),
            'hora': forms.TextInput(attrs={'class': 'form-control', 'type': 'time'}),
            'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
            'proposito': autocomplete.ModelSelect2(url='proposito-autocomplete', attrs={'class': 'form-control'}),
        }

    # def __init__(self, *args, **kwargs):
    #     super(TurnoForm, self).__init__(*args, **kwargs)
    #     self.fields['cliente'].queryset = Cliente.objects.filter(consumidor_final=False)


class FechasVentaForm(forms.Form):
    fecha_ini = forms.DateField(widget=forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}))
    fecha_fin = forms.DateField(widget=forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}))

    # class Meta:
    #     widgets = {
    #         'fecha_ini': forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}),
    #         'fecha_fin': forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'})
    #     }

