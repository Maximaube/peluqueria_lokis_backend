from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from apps.servicios.models import Servicio
from apps.productos.models import Producto
from apps.usuarios.models import Cliente

from .models import Venta, Turno
from .forms import VentaForm, TurnoForm

from dal import autocomplete

from django_filters.views import FilterView
from .filters import EstadoVentasRealizadasFilter, VentasFormaPagoFilter

from django.contrib import messages
from apps.ventas.impresora import impresora
import xlsxwriter



# ------------ Views Venta -----------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class VentaList(ListView):
    model = Venta
    template_name = 'ventas/VentasList.html'

    def get_context_data(self, **kwargs):
        context = super(VentaList, self).get_context_data(**kwargs)
        hoy = datetime.now()
        total_recaudado = 0
        ventas = Venta.objects.filter(fecha=hoy).order_by('-hora')
        for venta in ventas:
            total_recaudado = total_recaudado + venta.monto
        context['total_recaudado'] = total_recaudado
        context['listado_ventas'] = ventas
        context['cantidad_ventas_hoy'] = ventas.count()
        context['fecha_hoy'] = hoy.date
        context['VentaList'] = True
        context['menu_title'] = 'Ventas'
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class VentaDetail(DetailView):
    model = Venta
    template_name = 'ventas/VentaDetail.html'
    context_object_name = 'venta'


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class VentaCreate(CreateView):
    model = Venta
    form_class = VentaForm
    template_name = 'ventas/VentaForm.html'
    success_url = reverse_lazy('listado_ventas')
    extra_context = {'VentaCreate': True, 'menu_title': 'Ventas'}

    def post(self, request, *args, **kwargs):

        form = self.get_form()
        if form.is_valid():
            listado_servicios = request.POST.getlist('servicios') #Trae el listado de servicios desde el form
            listado_productos = request.POST.getlist('productos')# Hace lo mismo con los productos
            # Verifica que no este vacia la lista de servicios
            if len(listado_servicios) == 0 and len(listado_productos) == 0:#probemos este error, sin agregar servicios
                messages.error(request, "Debe agregar al menos un servicio o producto a la venta")
                return render(request, self.template_name, {'form': form})

            listado_para_imprimir = []
            for cod_servicio in listado_servicios:#El listado trae los codigos de cada servicio, por eso recorro con un for
                servicio = Servicio.objects.get(pk=cod_servicio)#y aqui hago una consulta para traer ese servicio
                listado_para_imprimir.append(servicio)#y lo agrego al listado para imprimir

            for cod_producto in listado_productos:#hago lo mismo con los productos
                producto = Producto.objects.get(pk=cod_producto)
                listado_para_imprimir.append(producto)

            # Si esta tildado para imprimir, llama a la impresora
            if request.POST.get('para_imprimir'):#verifico si esta tildado
                cliente = Cliente.objects.get(pk=request.POST.get('cliente'))
                error = impresora.ticket(listado_para_imprimir, cliente.get_name)#aqui llamo a imprimir, lo hice generico, le mando un listado
                # Si retorna 0 es porque imprimio bien, sino hubo algun error
                if error != 0:
                    # Convierto el numero de error a hexadecimal para ver en la tabla de errores
                    error = hex(error)
                    messages.error(request, "Error al imprimir. Codigo de error: "+str(error))#aqui podemos ver el codigo en hexa para saber q tipo de error es
                    return render(request, self.template_name, {'form': form})#si hubo error aqui vuelvo a la pantalla de ventas

            return self.form_valid(form)#entra por aqui si es que salio todo bien
        else:
            return self.form_invalid(form)#entra por aqui si es q hubo una mala validacion del form, lo hace solo el django

    def form_valid(self, form):
        instance = form.save(commit=False)
        # Si estaba tildado para imprimir y llego a este metodo es xq imprimio y cambio impreso a True
        if self.request.POST.get('para_imprimir'):
            instance.impreso = True
        instance.save()
        form.save_m2m()
        return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class VentaUpdate(UpdateView):
    model = Venta
    form_class = VentaForm
    template_name = 'ventas/VentaForm.html'
    success_url = reverse_lazy('listado_ventas')
    extra_context = {'VentaUpdate': True, 'menu_title': 'Ventas'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class VentaDelete(DeleteView):
    model = Venta
    template_name = 'base/ConfirmDelete.html'
    success_url = reverse_lazy('listado_ventas')
    context_object_name = 'objeto'
    extra_context = {'VentaDelete': True, 'menu_title': 'Ventas', 'tipo': 'la Venta de'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ReporteEstadoVentasRealizadas(FilterView):
    model = Venta
    context_object_name = 'ventas'
    filterset_class = EstadoVentasRealizadasFilter
    # paginate_by = 3
    template_name = 'reportes/ReporteEstadoVentasRealizadas.html'
    extra_context = {
                        'ReporteEstadoVentas': True,
                        'menu_title': 'Ventas'
                    }

    def get_context_data(self, **kwargs):
        context = super(ReporteEstadoVentasRealizadas, self).get_context_data(**kwargs)
        listado = list(self.object_list.values_list('id', flat=True))
        cache.set('listado_a_exportar', listado)
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ReporteVentasFormaPago(FilterView):
    model = Venta
    context_object_name = 'ventas'
    filterset_class = VentasFormaPagoFilter
    # paginate_by = 3
    template_name = 'reportes/ReporteVentasFormaPago.html'
    extra_context = {
                        'ReporteVentasFormaPago': True,
                        'menu_title': 'Ventas'
                    }

    def get_context_data(self, **kwargs):
        context = super(ReporteVentasFormaPago, self).get_context_data(**kwargs)
        context['cant_efectivo'] = Venta.objects.filter(metodo_pago__metodo='Efectivo').count()
        context['cant_debito'] = Venta.objects.filter(metodo_pago__metodo='Tarjeta débito').count()
        context['cant_credito'] = Venta.objects.filter(metodo_pago__metodo='Tarjeta crédito').count()
        return context


@login_required
def exportar_excel(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Reporte Ventas.xls"'

    workbook = xlsxwriter.Workbook(response)
    worksheet = workbook.add_worksheet()

    # Formateo los titulos
    title_format = workbook.add_format({'bold': True})
    title_format.set_bg_color('#bfb9b9')
    title_format.set_border(1)

    # Agrego los titulos a la primera fila
    worksheet.write('A1', 'Fecha', title_format)
    worksheet.write('B1', 'Hora', title_format)
    worksheet.write('C1', 'Cliente', title_format)
    worksheet.write('D1', 'Peluquero', title_format)
    worksheet.write('E1', 'Servicios', title_format)
    worksheet.write('F1', 'Productos', title_format)
    worksheet.write('G1', 'Total a pagar', title_format)
    worksheet.write('H1', 'Forma de pago', title_format)
    worksheet.write('I1', 'Descuento', title_format)
    worksheet.write('J1', 'Impresa', title_format)

    # Formateo las celdas
    cell_format = workbook.add_format()
    cell_format.set_border(1)
    worksheet.set_default_row(40, 40)

    # Traigo el listado de ids desde la cache
    listado = cache.get('listado_a_exportar')
    i = 2  # Inicio en 2 para escribir a partir de la segunda fila
    for id in listado:  # Recorro el listado para buscar cada una de las ventas con los ids
        venta = Venta.objects.get(id=id)
        worksheet.write('A{}'.format(i), str(venta.fecha.strftime("%d/%m/%Y")), cell_format)
        worksheet.write('B{}'.format(i), str(venta.hora.strftime("%H:%M")), cell_format)
        worksheet.write('C{}'.format(i), venta.cliente.get_name, cell_format)
        if venta.profesional:
            worksheet.write('D{}'.format(i), venta.profesional.get_name, cell_format)
        else:
            worksheet.write('D{}'.format(i), '', cell_format)
        string_servicios = ''   # Creo un string con todos los servicios de la venta con saltos de linea
        for servicio in venta.servicios.all():
            string_servicios = string_servicios + '- {}\n'.format(servicio.get_name)
        worksheet.write('E{}'.format(i), string_servicios, cell_format)

        string_productos = ''
        for producto in venta.productos.all():
            string_productos = string_productos + '- {}\n'.format(producto.get_name)
        worksheet.write('F{}'.format(i), string_productos, cell_format)
        worksheet.write('G{}'.format(i), venta.monto, cell_format)
        worksheet.write('H{}'.format(i), venta.metodo_pago.get_name, cell_format)
        worksheet.write('I{}'.format(i), venta.descuento, cell_format)
        if venta.impreso:
            worksheet.write('J{}'.format(i), 'SI', cell_format)
        else:
            worksheet.write('J{}'.format(i), 'NO', cell_format)

        i += 1

    workbook.close()
    return response

# ------------ Views Turno --------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class TurnoList(ListView):
    model = Turno
    template_name = 'turnos/TurnosList.html'

    def get_context_data(self, **kwargs):
        context = super(TurnoList, self).get_context_data(**kwargs)
        hoy = datetime.now()
        listado_turnos_hoy = [] # Tabla de turnos pendientes
        listado_turnos_hoy_atendidos = []   # Tabla de turnos ya atendidos
        turnos_pendientes = 0
        turnos_hoy = Turno.objects.filter(fecha=hoy).order_by('hora')
        for i, turno in enumerate(turnos_hoy):
            elemento = {}
            elemento['orden'] = i + 1
            elemento['turno'] = turno
            if turno.hora > hoy.time():
                listado_turnos_hoy.append(elemento)
                turnos_pendientes+=1
            else:
                listado_turnos_hoy_atendidos.append(elemento)
        context['total_turnos'] = turnos_hoy.count()
        context['turnos_pendientes'] = turnos_pendientes
        context['turnos_hoy'] = listado_turnos_hoy
        context['turnos_hoy_atendidos'] = listado_turnos_hoy_atendidos
        context['fecha_hoy'] = hoy.date
        context['TurnoList'] = True
        context['menu_title'] = 'Turnos'
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class TurnoDetail(DetailView):
    model = Turno
    template_name = 'turnos/TurnoDetail.html'
    context_object_name = 'turno'


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class TurnoCreate(CreateView):
    model = Turno
    form_class = TurnoForm
    template_name = 'turnos/TurnoForm.html'
    success_url = reverse_lazy('listado_turnos')
    extra_context = {'TurnoCreate': True, 'menu_title': 'Turnos'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class TurnoUpdate(UpdateView):
    model = Turno
    form_class = TurnoForm
    template_name = 'turnos/TurnoForm.html'
    success_url = reverse_lazy('listado_turnos')
    extra_context = {'TurnoUpdate': True, 'menu_title': 'Turnos'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class TurnoDelete(DeleteView):
    model = Turno
    template_name = 'base/ConfirmDelete.html'
    success_url = reverse_lazy('listado_turnos')
    context_object_name = 'objeto'
    extra_context = {'TurnoDelete': True, 'menu_title': 'Turnos', 'tipo': 'el Turno de'}


# ------------ Views Autocompletes -----------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ClienteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        # 	return Familiar.objects.none()

        qs = Cliente.objects.all()

        if self.q:
            qs = qs.filter(Q(nombre__icontains=self.q) | Q(apellido__icontains=self.q))

        return qs


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ServicioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        # 	return Familiar.objects.none()

        qs = Servicio.objects.all()

        if self.q:
            qs = qs.filter(Q(codigo__icontains=self.q) | Q(nombre__icontains=self.q))

        return qs

    def get_result_label(self, item):
        # return str(item.codigo+' - '+item.nombre)
        return item.nombre

    def get_selected_result_label(self, item):
        return item.codigo


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProductoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #   return Familiar.objects.none()

        qs = Producto.objects.all()

        if self.q:
            qs = qs.filter(Q(codigo__icontains=self.q) | Q(nombre__icontains=self.q))

        return qs

    def get_result_label(self, item):
        # return str(item.codigo+' - '+item.nombre)
        return item.nombre

    def get_selected_result_label(self, item):
        return item.codigo


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class PropositoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #   return Familiar.objects.none()

        qs = Servicio.objects.all()

        if self.q:
            qs = qs.filter(Q(codigo__icontains=self.q) | Q(nombre__icontains=self.q))

        return qs

    def get_result_label(self, item):
        # return str(item.codigo+' - '+item.nombre)
        return item.nombre

    def get_selected_result_label(self, item):
        return item.nombre
