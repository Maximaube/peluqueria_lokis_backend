"""peluqueria URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from .views import (VentaList, VentaCreate, VentaUpdate, VentaDelete,
                    TurnoList, TurnoCreate, TurnoUpdate,
                    TurnoDelete, ClienteAutocomplete, ServicioAutocomplete, ProductoAutocomplete, 
                    PropositoAutocomplete, ReporteEstadoVentasRealizadas, ReporteVentasFormaPago,
                    exportar_excel)

urlpatterns = [
    # --------- URLs Ventas --------------------------------------------
    path('venta/', VentaList.as_view(), name='listado_ventas'),
    path('venta/nuevo/', VentaCreate.as_view(), name='crear_venta'),
    path('venta/modificar/<int:pk>', VentaUpdate.as_view(), name='modificar_venta'),
    path('venta/eliminar/<int:pk>', VentaDelete.as_view(), name='eliminar_venta'),

    path('reportes/ventas-realizadas/', ReporteEstadoVentasRealizadas.as_view(), name='reporte_estado_ventas_realizadas'),
    path('reportes/ventas-forma-pago/', ReporteVentasFormaPago.as_view(), name='reporte_ventas_forma_pago'),
    path('reportes/exportar-excel/', exportar_excel, name='exportar_excel'),

    # --------- URLs Turnos ----------------------------------------
    path('turno/', TurnoList.as_view(), name='listado_turnos'),
    path('turno/nuevo/', TurnoCreate.as_view(), name='crear_turno'),
    path('turno/modificar/<int:pk>', TurnoUpdate.as_view(), name='modificar_turno'),
    path('turno/eliminar/<int:pk>', TurnoDelete.as_view(), name='eliminar_turno'),

    path('cliente-autocomplete/', ClienteAutocomplete.as_view(), name='cliente-autocomplete',),
    path('servicios-autocomplete/', ServicioAutocomplete.as_view(), name='servicios-autocomplete',),
    path('productos-autocomplete/', ProductoAutocomplete.as_view(), name='productos-autocomplete',),
    path('proposito-autocomplete/', PropositoAutocomplete.as_view(), name='proposito-autocomplete',),
]
