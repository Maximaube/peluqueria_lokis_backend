from datetime import datetime
from django.db import models

from apps.core.models import Signature
from apps.productos.models import Producto
from apps.servicios.models import Servicio
from apps.usuarios.models import Cliente, Profesional


class MetodoPago(models.Model):
    metodo = models.CharField(max_length=60)

    class Meta:
        verbose_name = ('Metodo de Pago')
        verbose_name_plural = ('Metodos de Pago')

    def __str__(self):
        return self.get_name

    @property
    def get_name(self):
        return '%s' % (self.metodo)


def cliente_default():
    obj, created = Cliente.objects.get_or_create(
        apellido = 'Consumidor Final',
        nombre = '.',
        fecha_nacimiento = '1900-01-01',
        telefono = '44444444',
        consumidor_final = True,
    )
    return obj


def default_metodo_pago():
    obj, created = MetodoPago.objects.get_or_create(
        metodo='Efectivo',
    )
    return obj


class Venta(Signature):
    cliente = models.ForeignKey(Cliente, on_delete=models.SET(cliente_default), default=cliente_default, null=True, related_name='ventas_cliente')
    fecha = models.DateField(default=datetime.now)
    hora = models.TimeField(default=datetime.now)
    profesional = models.ForeignKey(Profesional, on_delete=models.SET_NULL, blank=True, null=True, related_name='ventas_profesional')
    servicios = models.ManyToManyField(Servicio, blank=True, related_name='ventas_servicio')
    productos = models.ManyToManyField(Producto, blank=True, related_name='ventas_producto')
    metodo_pago = models.ForeignKey(MetodoPago, on_delete=models.SET_NULL, null=True, default=default_metodo_pago)
    descuento = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    monto = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    para_imprimir = models.BooleanField(default=True)
    impreso = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % (self.get_name)

    class Meta:
        ordering = ('-fecha', '-hora',)

    @property
    def get_name(self):
        return '{} el {} - {}'.format(
            self.cliente,
            self.fecha.strftime("%d/%m/%Y"),
            self.hora.strftime("%H:%M")
        )

    @property
    def cantidad_productos(self):
        return self.productos.count()

    @property
    def cantidad_servicios(self):
        return self.servicios.count()


class Turno(Signature):
    profesional = models.ForeignKey(Profesional, on_delete=models.SET_NULL, blank=True, null=True, related_name='turnos_profesional')
    cliente = models.ForeignKey(Cliente, on_delete=models.SET(cliente_default), default=cliente_default, null=True, related_name='turnos_cliente')
    fecha = models.DateField(default=datetime.now)
    hora = models.TimeField()
    descripcion = models.TextField('Descripcion', blank=True, null=True)
    proposito = models.ForeignKey(Servicio, on_delete=models.SET_NULL, blank=True, null=True, related_name='turnos_servicio')

    def __str__(self):
        return '%s' % (self.get_name)

    @property
    def get_name(self):
        return '{} el {} - {}'.format(
            self.cliente,
            self.fecha.strftime("%d/%m/%Y"),
            self.hora.strftime("%H:%M")
        )


