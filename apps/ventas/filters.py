import django_filters

from .models import Venta
from django import forms


class EstadoVentasRealizadasFilter(django_filters.FilterSet):
    fecha_desde = django_filters.DateFilter(field_name='fecha', lookup_expr=('gte'),
                                            widget=forms.DateInput(format="%Y-%m-%d",
                                            attrs={'class': 'form-control', 'type': 'date'}),)
    fecha_hasta = django_filters.DateFilter(field_name='fecha', lookup_expr=('lte'),
                                            widget=forms.DateInput(format="%Y-%m-%d",
                                            attrs={'class': 'form-control', 'type': 'date'}),)
    rango_fechas = django_filters.DateRangeFilter(field_name='fecha',
                                                  widget=forms.Select(attrs={'class': 'form-control'}))

    IMPRESAS_CHOICES = (
        ('', 'Todas'),
        ('1', 'Impresas'),
        ('0', 'No impresas'),

    )
    ventas_impresas = django_filters.ChoiceFilter(field_name='impreso', choices=IMPRESAS_CHOICES, empty_label=None,
                                                   widget=forms.Select(attrs={'class': 'form-control'}),)

    class Meta:
        model = Venta
        fields = ['fecha']


class VentasFormaPagoFilter(django_filters.FilterSet):
    fecha_desde = django_filters.DateFilter(field_name='fecha', lookup_expr=('gte'),
                                            widget=forms.DateInput(format="%Y-%m-%d",
                                            attrs={'class': 'form-control', 'type': 'date'}),)
    fecha_hasta = django_filters.DateFilter(field_name='fecha', lookup_expr=('lte'),
                                            widget=forms.DateInput(format="%Y-%m-%d",
                                            attrs={'class': 'form-control', 'type': 'date'}),)
    rango_fechas = django_filters.DateRangeFilter(field_name='fecha',
                                                  widget=forms.Select(attrs={'class': 'form-control'}))

    # Filtra las distintas opciones posibles de los Metodos de pago
    # Solamente las opciones que estan en las ventas, si alguna opcion no fue
    # elegida en una venta no saldra
    def opciones_metodo_pago():
        return Venta.objects.order_by().values_list('metodo_pago', 'metodo_pago__metodo').distinct()

    formas_pago = django_filters.ChoiceFilter(field_name='metodo_pago', choices=opciones_metodo_pago(), empty_label='Todas',
                                                   widget=forms.Select(attrs={'class': 'form-control'}),)

    class Meta:
        model = Venta
        fields = ['fecha', 'metodo_pago']

