from django.contrib import admin

from .models import Venta, Turno, MetodoPago


admin.site.register(MetodoPago)
admin.site.register(Venta)
admin.site.register(Turno)
