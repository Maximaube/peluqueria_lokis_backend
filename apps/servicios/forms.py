from django import forms

from .models import Servicio


class ServicioForm(forms.ModelForm):
    class Meta:
        model = Servicio
        fields = ('codigo', 'nombre', 'descripcion', 'precio')

        labels = {
            'codigo': 'Codigo',
            'nombre': 'Nombre',
            'descripcion': 'Descripcion',
            'precio': 'Precio',
        }

        widgets = {
            'codigo': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'descripcion': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'precio': forms.NumberInput(attrs={'class': 'form-control'}),
        }
