from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .models import Servicio
from .forms import ServicioForm


# ------------ Views Servicio -------------------------------------------------------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ServicioList(ListView):
    model = Servicio
    template_name = 'servicios/ServiciosList.html'
    context_object_name = 'listado_servicios'

    def get_context_data(self, **kwargs):
        context = super(ServicioList, self).get_context_data(**kwargs)
        context['cantidad_servicios'] = Servicio.objects.all().count()
        context['menu_title'] = 'Servicios'
        context['ServicioList'] = True
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ServicioCreate(CreateView):
    model = Servicio
    form_class = ServicioForm
    template_name = 'servicios/ServicioForm.html'
    success_url = reverse_lazy('listado_servicios')
    extra_context = {'ServicioCreate': True, 'menu_title': 'Servicios'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ServicioUpdate(UpdateView):
    model = Servicio
    form_class = ServicioForm
    template_name = 'servicios/ServicioForm.html'
    pk_url_kwarg = 'codigo'
    success_url = reverse_lazy('listado_servicios')
    extra_context = {'ServicioUpdate': True, 'menu_title': 'Servicios'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ServicioDelete(DeleteView):
    model = Servicio
    template_name = 'base/ConfirmDelete.html'
    pk_url_kwarg = 'codigo'
    success_url = reverse_lazy('listado_servicios')
    context_object_name = 'objeto'
    extra_context = {'ServicioDelete': True, 'menu_title': 'Servicios', 'tipo': 'el Servicio'}

