from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ServicioList, ServicioCreate, ServicioUpdate, ServicioDelete
from .api.view import ServicioViewSetReadOnly


router = DefaultRouter()
router.register('servicio', ServicioViewSetReadOnly, basename='servicio_api')

urlpatterns = [
    # --------- URLs Servicio --------------------------------------------
    path('', ServicioList.as_view(), name='listado_servicios'),
    path('nuevo/', ServicioCreate.as_view(), name='crear_servicio'),
    path('modificar/<str:codigo>', ServicioUpdate.as_view(), name='modificar_servicio'),
    path('eliminar/<str:codigo>', ServicioDelete.as_view(), name='eliminar_servicio'),

    path('api/v1/', include(router.urls)),
]
