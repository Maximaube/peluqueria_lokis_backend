from django.db import models

from apps.core.models import Signature


class Servicio(Signature):
    codigo = models.CharField(max_length=20, primary_key=True)
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField('Descripcion', blank=True)
    precio = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return '%s' % self.codigo

    @property
    def get_name(self):
        return '%s' % (self.nombre)

    class Meta:
        ordering = ('nombre',)
