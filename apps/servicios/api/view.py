from rest_framework import viewsets

from .serializers import ServicioSerializer
from ..models import Servicio


class ServicioViewSetReadOnly(viewsets.ReadOnlyModelViewSet):
    queryset = Servicio.objects.all()
    serializer_class = ServicioSerializer
