from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .models import Producto
from .forms import ProductoForm


# ------------ Views Producto -------------------------------------------------------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProductoList(ListView):
    model = Producto
    template_name = 'productos/ProductosList.html'
    context_object_name = 'listado_productos'

    def get_context_data(self, **kwargs):
        context = super(ProductoList, self).get_context_data(**kwargs)
        context['cantidad_productos'] = Producto.objects.all().count()
        context['menu_title'] = 'Productos'
        context['ProductoList'] = True
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProductoCreate(CreateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'productos/ProductoForm.html'
    success_url = reverse_lazy('listado_productos')
    extra_context = {'ProductoCreate': True, 'menu_title': 'Productos'}

    # def get_success_url(self, **kwargs):
    #     return reverse_lazy('detalle_producto', args=[self.object.codigo])

    # def form_valid(self, form):
    #     user = self.request.user
    #     instance = form.save(commit=False)
    #     if not instance.codigo:
    #         instance.created_by = user
    #         instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProductoUpdate(UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'productos/ProductoForm.html'
    pk_url_kwarg = 'codigo'
    success_url = reverse_lazy('listado_productos')
    extra_context = {'ProductoUpdate': True, 'menu_title': 'Productos'}

    # def get_success_url(self, **kwargs):
    #     print('Entra a update')
    #     return reverse_lazy('detalle_producto', args=[self.object.codigo])

    # def form_valid(self, form):
    #     user = self.request.user
    #     instance = form.save(commit=False)
    #     instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProductoDelete(DeleteView):
    model = Producto
    template_name = 'base/ConfirmDelete.html'
    pk_url_kwarg = 'codigo'
    success_url = reverse_lazy('listado_productos')
    context_object_name = 'objeto'
    extra_context = {'ProductoDelete': True, 'menu_title': 'Productos', 'tipo': 'el Producto'}
