from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ProductoList, ProductoCreate, ProductoUpdate, ProductoDelete
from .api.view import ProductoViewSetReadOnly

router = DefaultRouter()
router.register('producto', ProductoViewSetReadOnly, basename='producto_api')

urlpatterns = [
    # --------- URLs Producto --------------------------------------------
    path('', ProductoList.as_view(), name='listado_productos'),
    path('nuevo/', ProductoCreate.as_view(), name='crear_producto'),
    path('modificar/<str:codigo>', ProductoUpdate.as_view(), name='modificar_producto'),
    path('eliminar/<str:codigo>', ProductoDelete.as_view(), name='eliminar_producto'),

    path('api/v1/', include(router.urls)),
]
