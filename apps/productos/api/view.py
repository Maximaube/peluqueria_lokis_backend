from rest_framework import viewsets

from .serializers import ProductoSerializer
from ..models import Producto


class ProductoViewSetReadOnly(viewsets.ReadOnlyModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
