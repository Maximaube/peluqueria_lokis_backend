from django.db import models

from apps.core.models import Signature


class CategoriaProducto(models.Model):
    nombre_categoria = models.CharField(max_length=60)

    class Meta:
        verbose_name = ('Categoria Producto')
        verbose_name_plural = ('Categorias Producto')

    def __str__(self):
        return self.get_name

    @property
    def get_name(self):
        return '%s' % (self.nombre_categoria)


class Producto(Signature):
    codigo = models.CharField(max_length=20, primary_key=True)
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField('Descripcion', blank=True)
    categoria = models.ForeignKey(CategoriaProducto, on_delete=models.SET_NULL, null=True, blank=True)
    proveedor = models.CharField(max_length=200, blank=True)
    iva = models.DecimalField('% IVA', max_digits=8, decimal_places=2, blank=True, null=True)
    precio = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return '%s' % (self.codigo)

    @property
    def get_name(self):
        return '%s' % (self.nombre)

    class Meta:
        ordering = ('nombre',)