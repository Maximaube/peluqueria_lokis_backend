from django.db import models
from django.conf import settings
from .manager import ActivesManager


class Signature(models.Model):
    class Meta:
        abstract = True

    objects = models.Manager()
    actives = ActivesManager()

    is_active = models.BooleanField(
        default=True,
        editable=False
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        null=True,
        blank=True
    )
    modified_on = models.DateTimeField(
        auto_now=True,
        null=True,
        blank=True
    )

