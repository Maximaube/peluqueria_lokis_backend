from django.contrib import admin
from .models import Cliente, Profesional, ComoDescubrio


class ClienteAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'apellido']
    readonly_fields = ('created_on', 'modified_on',)
    list_display = ('apellido', 'nombre',)
    list_filter = ('apellido', 'nombre', 'sexo',)

    fieldsets = (
        (None,
         {'fields': ('apellido', 'nombre', 'dni', 'sexo', 'fecha_nacimiento', 'email', 'telefono', 'detalles', 'como_descubrio', 'hijo_de')}),
        ('Domicilio', {'fields': ('direccion', 'provincia')}),
        (None, {'fields': ('consumidor_final', 'created_on', 'modified_on',)}),
    )

    search_fields = ('apellido', 'nombre',)
    ordering = ('apellido', 'nombre',)

admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Profesional)
admin.site.register(ComoDescubrio)
