from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import FormView
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, TemplateView

from apps.usuarios.models import Cliente, Profesional, ComoDescubrio
from apps.ventas.models import Venta
from .forms import ClienteForm, ProfesionalForm, FechasForm


# ------------ Views Cliente -----------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ClienteList(ListView):
    model = Cliente
    template_name = 'usuarios/ClientesList.html'

    def get_context_data(self, **kwargs):
        context = super(ClienteList, self).get_context_data(**kwargs)
        listado = Cliente.objects.filter(consumidor_final=False)
        context['listado_clientes'] = listado
        context['cantidad_clientes'] = listado.count()
        context['menu_title'] = 'Clientes'
        context['ClienteList'] = True
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ClienteDetail(DetailView):
    model = Cliente
    template_name = 'usuarios/ClienteDetail.html'
    context_object_name = 'cliente'
    extra_context = {'menu_title': 'Clientes'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ClienteCreate(CreateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'usuarios/ClienteForm.html'
    success_url = reverse_lazy('listado_clientes')
    extra_context = {'ClienteCreate': True, 'menu_title': 'Clientes'}

    # def get_success_url(self, **kwargs):
    #     return reverse_lazy('detalle_cliente', args=[self.object.id])

    # def form_valid(self, form):
    #     user = self.request.user
    #     instance = form.save(commit=False)
    #     if not instance.pk:
    #         instance.created_by = user
    #         instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ClienteUpdate(UpdateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'usuarios/ClienteForm.html'
    success_url = reverse_lazy('listado_clientes')
    extra_context = {'ClienteUpdate': True, 'menu_title': 'Clientes'}

    # def form_valid(self, form):
    #     user = self.request.user
    #     instance = form.save(commit=False)
    #     instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ClienteDelete(DeleteView):
    model = Cliente
    template_name = 'base/ConfirmDelete.html'
    success_url = reverse_lazy('listado_clientes')
    context_object_name = 'objeto'
    extra_context = {'ClienteDelete': True, 'menu_title': 'Clientes', 'tipo': ' al Cliente'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class CumpleaniosList(ListView):
    model = Cliente
    template_name = 'calendario/Cumpleanios.html'

    def get_context_data(self, **kwargs):
        context = super(CumpleaniosList, self).get_context_data(**kwargs)
        hoy = datetime.now()

        # Listado de cumpleaños
        from apps.ventas.models import Venta
        filtrado = Cliente.objects.filter(fecha_nacimiento__month__gte=hoy.month, consumidor_final=False)
        # print(filtrado)
        listado = []
        for cliente in filtrado:
            if not (cliente.fecha_nacimiento.month==hoy.month and cliente.fecha_nacimiento.day < hoy.day):
                ventas = Venta.objects.filter(cliente=cliente)
                if ventas:
                    cliente.atenciones = ventas.count()
                    cliente.ultima_visita = ventas.latest('fecha').fecha
                else:
                    cliente.atenciones = '-'
                    cliente.ultima_visita = '-'
                listado.append(cliente)
        context['cumpleanios_list'] = listado

        context['fecha_hoy'] = hoy.date


        context['menu_title'] = 'Avisos de cumpleaños'
        context['CalendarioAvisosCumples'] = True
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ReporteComoConocieron(TemplateView):
        template_name = 'reportes/ReporteComoConocieron.html'

        def get_context_data(self, **kwargs):
            context = super(ReporteComoConocieron, self).get_context_data(**kwargs)

            listado = ComoDescubrio.objects.all()
            lista = []
            for opcion in listado:
                cantidad = Cliente.objects.filter(como_descubrio=opcion).count()
                elemento = {'label': opcion, 'cantidad': cantidad}
                lista.append(elemento)

            context['listado'] = lista
            context['menu_title'] = '¿Cómo conocieron a LOKIS?'
            context['ReporteComoConocieron'] = True
            return context


# ------------ Views Profesional --------------------
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProfesionalList(ListView):
    model = Profesional
    template_name = 'usuarios/ProfesionalList.html'
    context_object_name = 'listado_profesionales'

    def get_context_data(self, **kwargs):
        context = super(ProfesionalList, self).get_context_data(**kwargs)

        context['cantidad_profesionales'] = Profesional.objects.all().count()
        context['menu_title'] = 'Profesionales'
        context['ProfesionalList'] = True
        return context


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProfesionalDetail(DetailView):
    model = Profesional
    template_name = 'usuarios/ProfesionalDetail.html'
    context_object_name = 'profesional'


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProfesionalCreate(CreateView):
    model = Profesional
    form_class = ProfesionalForm
    template_name = 'usuarios/ProfesionalForm.html'
    success_url = reverse_lazy('listado_profesionales')
    extra_context = {'ProfesionalCreate': True}

    # def get_success_url(self, **kwargs):
    #     return reverse_lazy('detalle_profesional', args=[self.object.id])

    # def form_valid(self, form):
    #     user = self.request.user
    #     instance = form.save(commit=False)
    #     if not instance.pk:
    #         instance.created_by = user
    #         instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProfesionalUpdate(UpdateView):
    model = Profesional
    form_class = ProfesionalForm
    template_name = 'usuarios/ProfesionalForm.html'
    success_url = reverse_lazy('listado_profesionales')
    extra_context = {'ProfesionalUpdate': True, 'menu_title': 'Profesionales'}

    # def get_success_url(self, **kwargs):
    #     return reverse_lazy('detalle_profesional', args=[self.object.id])

    # def form_valid(self, form):
    #     user = self.request.user
    #     instance = form.save(commit=False)
    #     instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ProfesionalDelete(DeleteView):
    model = Profesional
    template_name = 'base/ConfirmDelete.html'
    success_url = reverse_lazy('listado_profesionales')
    context_object_name = 'objeto'
    extra_context = {'ProfesionalUpdate': True, 'menu_title': 'Profesionales', 'tipo': 'al Profesional'}


@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class ReportePeluqueros(FormView):
    form_class = FechasForm
    template_name = 'reportes/ReportePeluqueros.html'
    success_url = reverse_lazy('reporte_peluqueros')
    plus_context = dict()

    def get_context_data(self, **kwargs):
        context = super(ReportePeluqueros, self).get_context_data(**kwargs)

            # listado = ComoDescubrio.objects.all()
            # lista = []
            # for opcion in listado:
            #     cantidad = Cliente.objects.filter(como_descubrio=opcion).count()
            #     elemento = {'label': opcion, 'cantidad': cantidad}
            #     lista.append(elemento)
            #
            # context['listado'] = lista
        # context['desde'] = '05/01/2015'
        # print(datetime.now().date())
        if not self.plus_context:
            self.plus_context['desde'] = datetime.now().date()
            self.plus_context['hasta'] = datetime.now().date()

        context['fechas'] = self.plus_context
        lista = []
        for prof in Profesional.objects.all():
            cantidad = prof.ventas_profesional.filter(fecha__gte=self.plus_context['desde'], fecha__lte=self.plus_context['hasta']).count()
            elemento = {'label': prof.get_name, 'cantidad': cantidad}
            lista.append(elemento)
            # print(prof, prof.ventas_profesional.filter(fecha__gte=self.plus_context['desde'], fecha__lte=self.plus_context['hasta']).count())
        context['listado'] = lista
        context['menu_title'] = 'Desempeño de Peluqueros'
        context['ReportePeluqueros'] = True
        return context

    def form_valid(self, form):
        self.plus_context['desde'] = form.cleaned_data.get('fecha_desde')
        self.plus_context['hasta'] = form.cleaned_data.get('fecha_hasta')
        return super().form_valid(form)
