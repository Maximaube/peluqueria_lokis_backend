# -*- coding: utf-8 -*-
from django import forms

from .models import Cliente, Profesional


class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ('apellido', 'nombre', 'dni', 'sexo', 'fecha_nacimiento', 'email', 'telefono',
                  'direccion', 'provincia', 'como_descubrio', 'hijo_de', 'detalles')

        labels = {
            'apellido': 'Apellido',
            'nombre': 'Nombre',
            'dni': 'DNI N°',
            'sexo': 'Sexo',
            'fecha_nacimiento': 'Fecha de Nacimiento',
            'email': 'Email',
            'telefono': 'Telefono',
            'direccion': 'Direccion',
            'provincia': 'Provincia',
            'como_descubrio': '¿Como descubrio la peluqueria?',
            'hijo_de': 'Es hijo/a de',
            'detalles': 'Detalles extras',
        }

        widgets = {
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'dni': forms.NumberInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'sexo': forms.Select(attrs={'class': 'form-control'}),
            'fecha_nacimiento': forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'provincia': forms.Select(attrs={'class': 'form-control'}),
            'como_descubrio': forms.Select(attrs={'class': 'form-control'}),
            'hijo_de': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'detalles': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Le gusta jugar, llora mucho, etc.'}),
        }


class ProfesionalForm(forms.ModelForm):
    class Meta:
        model = Profesional
        fields = ('apellido', 'nombre', 'dni', 'fecha_nacimiento', 'telefono', 'direccion')

        labels = {
            'apellido': 'Apellido',
            'nombre': 'Nombre',
            'dni': 'DNI N°',
            'fecha_nacimiento': 'Fecha de Nacimiento',
            'telefono': 'Telefono',
            'direccion': 'Direccion',
        }

        widgets = {
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'dni': forms.NumberInput(attrs={'class': 'form-control', 'maxlength': '10'}),
            'fecha_nacimiento': forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
        }


class FechasForm(forms.Form):
    fecha_desde = forms.DateField(widget=forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}))
    fecha_hasta = forms.DateField(widget=forms.DateInput(format="%Y-%m-%d", attrs={'class': 'form-control', 'type': 'date'}))
