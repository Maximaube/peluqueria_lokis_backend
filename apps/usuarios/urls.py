"""peluqueria URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from apps.usuarios.views import (ClienteList, ClienteCreate,
                                 ClienteUpdate, ClienteDelete, CumpleaniosList,
                                 ProfesionalList, ProfesionalCreate, ProfesionalUpdate,
                                 ProfesionalDelete, ReporteComoConocieron, ReportePeluqueros)

urlpatterns = [
    # --------- URLs Cliente --------------------------------------------
    path('cliente/', ClienteList.as_view(), name='listado_clientes'),
    path('cliente/nuevo/', ClienteCreate.as_view(), name='crear_cliente'),
    path('cliente/modificar/<int:pk>', ClienteUpdate.as_view(), name='modificar_cliente'),
    path('cliente/eliminar/<int:pk>', ClienteDelete.as_view(), name='eliminar_cliente'),

    path('calendario/', CumpleaniosList.as_view(), name='avisos_cumpleanios'),
    path('reportes/como-conocieron/', ReporteComoConocieron.as_view(), name='reporte_como_conocieron'),
    path('reportes/peluqueros/', ReportePeluqueros.as_view(), name='reporte_peluqueros'),

    # --------- URLs Profesional ----------------------------------------
    path('profesional/', ProfesionalList.as_view(), name='listado_profesionales'),
    path('profesional/nuevo/', ProfesionalCreate.as_view(), name='crear_profesional'),
    path('profesional/modificar/<int:pk>', ProfesionalUpdate.as_view(), name='modificar_profesional'),
    path('profesional/eliminar/<int:pk>', ProfesionalDelete.as_view(), name='eliminar_profesional'),
]
