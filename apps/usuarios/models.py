import datetime
from django.db import models
from apps.core.models import Signature


class ComoDescubrio(models.Model):
    como_descubrio = models.CharField(max_length=60)

    class Meta:
        verbose_name = ('Como descubrio')
        verbose_name_plural = ('Como descubrio')

    def __str__(self):
        return self.como_descubrio


class Cliente(Signature):
    SEXO = (
        ('F', 'Femenino'),
        ('M', 'Masculino'),
    )
    PROVINCIAS = (
        ('BA', 'Buenos Aires'),
        ('CA', 'Catamarca'),
        ('CH', 'Chaco'),
        ('CT', 'Chubut'),
        ('CB', 'Cordoba'),
        ('CR', 'Corrientes'),
        ('ER', 'Entre Rios'),
        ('FO', 'Formosa'),
        ('JY', 'Jujuy'),
        ('LP', 'La Pampa'),
        ('LR', 'La Rioja'),
        ('MZ', 'Mendoza'),
        ('MI', 'Misiones'),
        ('NQ', 'Neuquen'),
        ('RN', 'Rio Negro'),
        ('SA', 'Salta'),
        ('SJ', 'San Juan'),
        ('SL', 'San Luis'),
        ('SC', 'Santa Cruz'),
        ('SF', 'Santa Fe'),
        ('SE', 'Santiago del Estero'),
        ('TF', 'Tierra del Fuego'),
        ('TU', 'Tucuman'),
    )

    apellido = models.CharField(max_length=200)
    nombre = models.CharField(max_length=200)
    dni = models.CharField('DNI', max_length=10, blank=True)
    sexo = models.CharField(max_length=2, choices=SEXO, blank=True)
    fecha_nacimiento = models.DateField('Fecha de nacimiento')
    email = models.EmailField(max_length=80, blank=True)
    telefono = models.CharField('Telefono', max_length=30)
    direccion = models.CharField(max_length=200, blank=True)
    provincia = models.CharField(max_length=3, choices=PROVINCIAS, default='CA', blank=True)
    como_descubrio = models.ForeignKey(ComoDescubrio, on_delete=models.SET_NULL, null=True, blank=True)
    hijo_de = models.CharField(max_length=80, blank=True)
    detalles = models.CharField('Detalles extras', max_length=200, blank=True)
    consumidor_final = models.BooleanField('Es consumidor final', default=False)


    @property
    def get_name(self):
        return '%s %s' % (self.apellido, self.nombre)

    def __str__(self):
        return '%s' % (self.get_name)

    class Meta:
        ordering = ('apellido', 'nombre',)

    @property
    def edad(self):
        """ retorna la edad si es que tiene cargada una fecha de nacimiento"""
        if self.fecha_nacimiento:
            """
                Calcula la edad exacta de la persona tomando en cuenta
                día, mes y año actual y mes, día y año de nacimiento.
            """
            # Obtenemos la fecha de hoy:   hoy = date.today()
            hoy = datetime.date.today()
            # Sustituimos el año de nacimiento por el actual:
            try:
                cumpleanios = self.fecha_nacimiento.replace(year=hoy.year)
                # En caso de que la fecha de nacimiento es 29 de
            # febrero y el año actual no sea bisiesto:
            except ValueError:
                # Le restamos uno al día de nacimiento para que quede en 28:
                cumpleanios = self.fecha_nacimiento.replace(year=hoy.year, day=self.fecha_nacimiento.day - 1)
                # Cálculo final:
            if cumpleanios > hoy:
                return hoy.year - self.fecha_nacimiento.year - 1
            else:
                return hoy.year - self.fecha_nacimiento.year
        else:
            return ''

    @property
    def cumpleanios(self):
        '''Esta funcion sirve para saber cuales son los cumpleaños restantes del año'''
        if self.fecha_nacimiento:
            hoy = datetime.date.today()
            try:
                cumpleanios = self.fecha_nacimiento.replace(year=hoy.year)
            except ValueError:
                cumpleanios = self.fecha_nacimiento.replace(year=hoy.year, day=self.fecha_nacimiento.day - 1)
            return cumpleanios
        else:
            return ''


class Profesional(Signature):
    apellido = models.CharField(max_length=200)
    nombre = models.CharField(max_length=200)
    dni = models.CharField('DNI', max_length=10, unique=True)
    fecha_nacimiento = models.DateField('Fecha de nacimiento')
    telefono = models.CharField('Telefono', max_length=30)
    direccion = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name_plural = ('Profesionales')

    @property
    def get_name(self):
        return '%s %s' % (self.apellido, self.nombre)

    def __str__(self):
        return '%s' % (self.get_name)

    class Meta:
        ordering = ('apellido', 'nombre',)

