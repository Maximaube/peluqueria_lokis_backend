from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import LoginView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic

from .models import User
from .forms import UserAdminCreationForm, LoginForm


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Su contraseña fue cambiada exitosamente!')
            return redirect('cambiar_pass')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'cuentas/cambiar_contrasenia.html', {
        'form': form
    })


@method_decorator(user_passes_test(lambda u:u.is_admin, login_url=reverse_lazy('login')), name='dispatch')
class UserList(generic.ListView):
    model = User
    template_name = 'cuentas/UsersList.html'
    context_object_name = 'listado_usuarios'

    def get_context_data(self, **kwargs):
        context = super(UserList, self).get_context_data(**kwargs)
        context['menu_title'] = 'Cuentas'
        context['UsuariosList'] = True
        return context


@method_decorator(user_passes_test(lambda u:u.is_admin, login_url=reverse_lazy('login')), name='dispatch')
class UserCreate(generic.CreateView):
    form_class = UserAdminCreationForm
    success_url = reverse_lazy('listado_usuarios')
    template_name = 'cuentas/UserForm.html'
    extra_context = {'UsuarioCreate': True, 'menu_title': 'Cuentas'}


@method_decorator(user_passes_test(lambda u:u.is_admin, login_url=reverse_lazy('login')), name='dispatch')
class UserUpdate(generic.UpdateView):
    model = User
    form_class = UserAdminCreationForm
    template_name = 'cuentas/UserForm.html'
    success_url = reverse_lazy('listado_usuarios')

    def get_context_data(self, **kwargs):
        context = super(UserUpdate, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        usuario = self.model.objects.get(id=pk) 
        context['contrasenia'] = usuario.contrasenia
        context['menu_title'] = 'Cuentas'
        context['UsuarioUpdate'] = True
        return context


@method_decorator(user_passes_test(lambda u:u.is_admin, login_url=reverse_lazy('login')), name='dispatch')
class UserDelete(generic.DeleteView):
    model = User
    template_name = 'base/ConfirmDelete.html'
    success_url = reverse_lazy('listado_usuarios')
    context_object_name = 'objeto'
    extra_context = {'UsuarioDelete': True, 'menu_title': 'Cuentas', 'tipo': ' al Usuario'}


class Login(LoginView):
    form_class = LoginForm
    template_name = 'cuentas/login.html'
