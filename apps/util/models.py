from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
# from django.contrib.auth.models import PermissionsMixin
# from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import UserManager
from django.utils import timezone


class UserManager(BaseUserManager):
    def create_user(self, usuario, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not usuario:
            raise ValueError('El campo Usuario es obligatorio')
        if not password:
            raise ValueError('El campo Password es obligatorio')
        user_obj = self.model(
            usuario=usuario,
        )
        user_obj.set_password(password)
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, usuario, contrasenia=None):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            usuario,
            contrasenia=contrasenia,
            is_staff=True
        )
        return user

    def create_superuser(self, usuario, password=None):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            usuario,
            password=password,
        )
        user.contrasenia = password
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


# class Usuario(AbstractBaseUser, PermissionsMixin):
class User(AbstractBaseUser):
    usuario = models.CharField(max_length=255, unique=True)
    contrasenia = models.CharField(max_length=255)
    fecha_creacion = models.DateTimeField(_('date joined'), default=timezone.now)
    active = models.BooleanField(default=True)
    admin = models.BooleanField(default=False)
    staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'usuario'
    # REQUIRED_FIELDS = ['contrasenia']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active

    @property
    def get_name(self):
        return '%s' % (self.usuario)

    def __str__(self):
        return '%s' % (self.get_name)