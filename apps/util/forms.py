from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
# users/forms.py
from .models import User


class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated contrasenia."""
    contrasenia1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    contrasenia2 = forms.CharField(label='Confirmar Contraseña', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    # admin = forms.BooleanField(label='Asignar como Administrador',
    #                            widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'type': 'checkbox'}),
    #                            required=False)

    class Meta:
        model = User
        fields = ('usuario', 'admin')

        widgets = {
            'usuario': forms.TextInput(attrs={'class': 'form-control'}),
            'admin': forms.CheckboxInput(attrs={'class': 'form-check-input', 'type': 'checkbox'}),
        }

    def clean_contrasenia2(self):
        # Check that the two contrasenia entries match
        contrasenia1 = self.cleaned_data.get("contrasenia1")
        contrasenia2 = self.cleaned_data.get("contrasenia2")
        if contrasenia1 and contrasenia2 and contrasenia1 != contrasenia2:
            raise forms.ValidationError("Las contraseñas no son iguales")
        return contrasenia2

    def save(self, commit=True):
        # Save the provided contrasenia in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["contrasenia1"])
        user.contrasenia = self.cleaned_data["contrasenia1"]
        if self.cleaned_data["admin"]:
            user.staff = True
            user.admin = True
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the contrasenia field with admin's
    contrasenia hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('usuario', 'contrasenia', 'admin')

    def clean_contrasenia(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["contrasenia"]


class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Usuario'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Contraseña'}))


