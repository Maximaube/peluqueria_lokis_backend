from datetime import datetime

from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView


# @user_passes_test(lambda u:u.is_staff, login_url=reverse_lazy('foo'))
@method_decorator(login_required(login_url=reverse_lazy('login')), name='dispatch')
class Home(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)
        from apps.usuarios.models import Cliente, Profesional
        from apps.ventas.models import Venta, Turno
        hoy = datetime.now()

        # Listado de cumpleaños
        filtrado = Cliente.objects.filter(fecha_nacimiento__day=hoy.day, fecha_nacimiento__month=hoy.month, consumidor_final=False)
        listado = []
        for cliente in filtrado:
            ventas = Venta.objects.filter(cliente=cliente)
            if ventas:
                cliente.atenciones = ventas.count()
                cliente.ultima_visita = ventas.latest('fecha').fecha
            else:
                cliente.atenciones = '-'
                cliente.ultima_visita = '-'
            listado.append(cliente)
        context['cumpleanios_list'] = listado

        # Genero el listado con los turnos restantes del dia
        listado_turnos_hoy = []
        turnos_hoy = Turno.objects.filter(fecha=hoy).order_by('hora')
        for i, turno in enumerate(turnos_hoy):
            if turno.hora > hoy.time():
                elemento = {}
                elemento['orden'] = i + 1
                elemento['turno'] = turno
                listado_turnos_hoy.append(elemento)
        context['turnos_hoy'] = listado_turnos_hoy
        context['fecha_hoy'] = hoy.date

        total_recaudado = 0
        ventas = Venta.objects.filter(fecha=hoy).order_by('-hora')
        for venta in ventas:
            total_recaudado = total_recaudado + venta.monto
        context['total_recaudado'] = total_recaudado
        context['total_turnos'] = turnos_hoy.count()
        context['cantidad_ventas_hoy'] = ventas.count()
        context['cantidad_profesionales'] = Profesional.objects.all().count()

        context['menu_title'] = 'Resumen del dia'
        context['Index'] = True
        return context


class Error404(TemplateView):
    template_name = 'base/error_404.html'